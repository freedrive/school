#include <iostream>

int main(){
  size_t length;
  std::cout << "how long is the array? " << std::endl;
  std::cin >> length;
  int* array = new int[length];
  std::cout << "enter the array" << std::endl;
  for (int i = 0; i < length; i++ ) {std::cin >> array[i]; std::cout << std::endl;}
  for (int i = 0; i < length; i++ )
    for (int j = 2; j < array[i]; j++)
      if (!(array[i] % j)){array[i] = 0; break;}
  std::cout << "here is the array: " << std::endl;
  for (int i = 0; i < length; i++ ) std::cout << array[i] << std::endl;
  delete[] array;
}
