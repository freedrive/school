#include <iostream>


int main(){
  double length, numero = 0;
  std::cout << "lunghezza array?" << std::endl;
  std::cin >> length;
  double* array = new double[length];
  double media = 0, buffer;
  std::cout << "inserisci i numeri:" << std::endl;
  for (int i = 0; i < length; i++) {std::cin >> array[i]; std::cout << std::endl;}
  for (int i = 0; i < length; i++) media += array[i];
  media /= length;
  buffer = array[0];
  for (int i = 1; i < length; i++)
    if (buffer > abs(array[i] - media)){
      numero = i;
      buffer = abs(array[i] - media);
    }
  std::cout << "la media è " << media << " e il numero che si è avvicinato di più è stato il " << numero+1 << "o, ovvero " << array[(int)numero] << std::endl;
  delete[] array;
}
