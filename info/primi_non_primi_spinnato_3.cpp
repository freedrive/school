#include <cmath>
#include <iostream>

void swap(int* numeri, int primo, int secondo){
	if (primo == secondo) return;
	int temp;
	temp = numeri[primo];
	numeri[primo] = numeri[secondo];
	numeri[secondo] = temp;
}

int minore(int* numeri, int lunghezza, int da_dove = 0){
	int min = da_dove;
	for (int i = da_dove; i < lunghezza; i++)
		if (numeri[i] < numeri[min])
			min = i;
	return min;
}

void sort(int* numeri, int lunghezza){
	for (int i = 0; i < lunghezza - 1; i++){
		swap(numeri, i, minore(numeri, lunghezza, i));
	}
}


int main() {
	int lunghezza, num_non_primi = 0;
	std::cout << "quanti numeri? ";
	std::cin >> lunghezza;
	int* numeri = new int[lunghezza];
	std::cout << "inserisci i numeri" << std::endl;
	for (int i = 0; i < lunghezza; i++) std::cin >> numeri[i];
	for (int i = 0; i < lunghezza; i++) {
		for (int j = 2; j < sqrt(numeri[i]); i++)
			if (!(numeri[i]%j)){
				num_non_primi++;
				break;
			}
	}

	int* primi = new int[lunghezza - num_non_primi];
	int* non_primi = new int[num_non_primi];

	for (int i = 0; i < lunghezza; i++) {
		for (int j = 2; j < sqrt(numeri[i]); j++)
			if (!(numeri[i]%j)){
				non_primi[i] = numeri[i];
				goto END;
			}
		primi[i] = numeri[i];
		END:;
	}

	sort(primi, lunghezza);
	sort(non_primi, lunghezza);

	for (int i = 0; i < lunghezza - num_non_primi; i++) numeri[i] = primi[i];
	for (int i = lunghezza - num_non_primi; i < lunghezza; i++) numeri[i] = non_primi[i - (lunghezza - num_non_primi)];

	std::cout << "[ ";
	for (int i = 0; i < lunghezza; i++) std::cout << numeri[i] << ", ";
	std::cout << "]" << std::endl;
}
