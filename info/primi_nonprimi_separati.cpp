#include <cmath>
#include <iostream>

class resizable_array {
	int _lunghezza;
	int* _contenuti;
public:
	resizable_array() : _lunghezza(0) {}
	resizable_array(int lunghezza) : _lunghezza(lunghezza) { _contenuti = new int(_lunghezza); }
	int get(int where) const { if (where <= _lunghezza) return _contenuti[where]; return NULL; }
	void set(int what, int where) { if (where <= _lunghezza) _contenuti[where] = what; }
	void add(int what) {
		if (!_lunghezza) {
			int* _contenuti = new int[1];
			_contenuti[0] = what;
			_lunghezza++;
			return;
		}
		int* temp = new int[_lunghezza];
		for (int i = 0; i < _lunghezza; i++) temp[i] = _contenuti[i];
		int* _contenuti = new int[_lunghezza + 1];
		for (int i = 0; i < _lunghezza; i++) _contenuti[i] = temp[i];
		_contenuti[_lunghezza] = what;
		_lunghezza++;
	}
	int lunghezza() const { return _lunghezza; }
	void sort() {
		int temp, minore_temp;
		for (int i = 0; i < _lunghezza - 1; i++) {
			minore_temp = minore(i);
			if (minore_temp == i) continue;
			temp = _contenuti[i];
			_contenuti[i] = _contenuti[minore_temp];
			_contenuti[minore_temp] = temp;
		}
	}
	int minore() {
		int minore = 0;
		for (int i = 1; i < _lunghezza; i++)
			if (_contenuti[minore] > _contenuti[i])
				minore = i;
		return minore;
	}
	int minore(int startfrom) {
		int minore = startfrom;
		for (int i = startfrom + 1; i < _lunghezza; i++)
			if (_contenuti[minore] > _contenuti[i])
				minore = i;
		return minore;
	}
	resizable_array trova(int what) {
		resizable_array posizioni;
		for (int i = 0; i < _lunghezza; i++)
			if (_contenuti[i] == what)
				posizioni.add(i);
		return posizioni;
	}

	friend std::ostream& operator<<(std::ostream& os, const resizable_array& array) {
		os << "[ ";
		for (int i = 0; i < array.lunghezza(); i++) {
			os << array.get(i);
			os << ", ";
		}
		os << "]";
		return os;
	}
};

int main() {
	int lunghezza, temp;
	std::cout << "quanta è lunga l'array? ";
	std::cin >> lunghezza;
	resizable_array numeri, primi, non_primi;
	std::cout << "inserisci i numeri" << std::endl;
	for (int i = 0; i < lunghezza; i++) { std::cin >> temp; numeri.add(temp); }
	for (int i = 0; i < lunghezza; i++) {
		for (int j = 2; j < sqrt(numeri.get(i)); j++) {
			if (!(numeri.get(i) % j)) {
				non_primi.add(numeri.get(i));
				goto END;
			}
		}
		primi.add(numeri.get(i));
	END:;
	}
	primi.sort();
	non_primi.sort();
	for (int i = 0; i < primi.lunghezza(); i++)
		numeri.set(primi.get(i), i);
	for (int i = primi.lunghezza(); i < numeri.lunghezza(); i++)
		numeri.set(non_primi.get(i - primi.lunghezza()), i);

	std::cout << numeri << std::endl;
}