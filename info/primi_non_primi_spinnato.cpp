#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

int main() {
	std::vector<int> numeri, primi, non_primi;
	int quanti, temp;
	std::cout << "quanti numeri? ";
	std::cin >> quanti;
	std::cout << "inserisci i numeri " << std::endl;

	for (int i = 0; i < quanti; i++) {std::cin >> temp; numeri.emplace_back(temp);}

	for (int i : numeri){
		for (int j = 0; j < sqrt(i); j++)
			if (!(i%j)) {
				non_primi.emplace_back(i);
				goto END;
			}

		primi.emplace_back(i);
		END:;
	}

	std::sort(primi.begin(), primi.end());
	std::sort(non_primi.begin(), non_primi.end());
	
	for (int i = 0; i < primi.size(); i++) numeri.assign(i, primi.at(i));
	for (int i = primi.size(); i < primi.size() + non_primi.size(); i++) numeri.assign(i, non_primi.at(i - primi.size()));

	std::cout << "[ ";
	for (int i : numeri) std::cout << i << ", ";
	std::cout << "]" << std::endl;
}
