#include <iostream>

int lowest(int* tosort, int length, int startfrom){
	int position = startfrom;
	int lowest = tosort[startfrom];
	for (int i = startfrom + 1; i < length; i++){
		if (lowest > tosort[i]) {
			lowest = tosort[i];
			position = i;
		}
	}
	return position;
}

int main() {
	int length, temp, position;
	std::cout << "how long is the array?" << std::endl;
	std::cin >> length;
	int* input = new int[length];
	
	for (int i = 0; i < length; i++) {
		std::cout << i << ": ";
		std::cin >> input[i];
	}
	
	for (int i = 0; i < length - 1; i++){
		int position = lowest(input, length, i);
		if (position != i){
			temp = input[i];
			input[i] = input[position];
			input[position] = temp;
		}
	}

	for (int i = 0; i < length; i++) std::cout << input[i] << std::endl;
	
	delete[] input;
}
