

assonometria cavaliera o obliqua -> profondità dimezzata
                                    altezza e larghezze uguale
                                    prolungamento x = linea terra

assonometria monometrica ->         altezza, larghezza e profondita invariata
                                    larghezza 30 gradi rispetto alle x

assonometria isometrica ->          le misure sugli assi rimangono invariate
